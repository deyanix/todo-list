/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.scss in this case)
import './styles/app.scss';

const $ = require('jquery');
const u2f = require('u2f-api');
require('bootstrap');
window.$ = $;

const $form = $('#form-u2f')
const $inputRegisterData = $('#input-register-data');
const $inputRegisterResponse = $('#input-register-response');
const $inputName = $('#input-name');
const $buttonApply = $('#button-apply');
const $registrationProgressBar = $('#registration-progress-bar > div:first-child');
const $registrationFeedback = $('#registration-feedback')

$buttonApply.on('click', function () {
	const registerDataText = $inputRegisterData.val();
	const registerData = JSON.parse(atob(registerDataText));
	const req = registerData.registerRequest;
	const sigs = registerData.signRequests;

	$registrationFeedback.text("Rejestracja klucza...");
	$registrationProgressBar.css('width', '50%');
	$registrationProgressBar.removeClass('bg-success bg-danger');
	$inputName.prop('disabled', true);
	$buttonApply.prop('disabled', true);

	u2f.register(req, sigs)
		.then(data => {
			const registerResponseText = btoa(JSON.stringify(data));
			$inputRegisterResponse.val(registerResponseText);

			$registrationFeedback.text('Pomyślnie zarejestrowano klucz.');
			$registrationProgressBar.css('width', '100%');
			$registrationProgressBar.addClass('bg-success');
			setTimeout(function () {
				$inputName.prop('disabled', false);
				$form.submit();
			}, 1000);
		})
		.catch(data => {
			$registrationFeedback.text('Wystąpił błąd podczas rejestracji klucza. Proszę spróbować ponowie.');
			$registrationProgressBar.css('width', '100%');
			$registrationProgressBar.addClass('bg-danger');
			$inputName.prop('disabled', false);
			$buttonApply.prop('disabled', false);
		});
});

// --- //

const $formAuthenticateU2f = $('#form-authenticate-u2f');
const $buttonAuthenticate = $('#button-authenticate');
const $inputAuthenticateData = $('#input-authenticate-data');
const $inputAuthenticateResponse = $('#input-authenticate-response');
const $u2fAuthenticateFeedback = $('#u2f-authenticate-feedback');

$buttonAuthenticate.on('click', function () {
	const authenticateDataText = $inputAuthenticateData.val();
	const authenticateData = JSON.parse(atob(authenticateDataText));
	$buttonAuthenticate.prop('disabled', true);

	u2f.sign(authenticateData)
		.then(data => {
			const authenticateResponseText = btoa(JSON.stringify(data));
			$inputAuthenticateResponse.val(authenticateResponseText);
			$formAuthenticateU2f.submit();
		})
		.catch(data => {
			$u2fAuthenticateFeedback.text('Błąd uwierzytelniania. Proszę spróbować ponownie.');
			$buttonAuthenticate.prop('disabled', false);
		})
});
