<?php

namespace App\Module\TwoFactor\Totp;

class TotpRegistrationEffect {
	private string $name;
	private string $secret;
	private string $code;

	public function __construct(string $name, string $secret, string $code) {
		$this->name = $name;
		$this->secret = $secret;
		$this->code = $code;
	}

	public function getName(): string {
		return $this->name;
	}

	public function getRealSecret(): string {
		return $this->secret;
	}

	public function getSecret(): string {
		return str_replace(' ', '', $this->getRealSecret());
	}

	public function getCode(): string {
		return $this->code;
	}
}
