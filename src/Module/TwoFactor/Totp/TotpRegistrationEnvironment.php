<?php

namespace App\Module\TwoFactor\Totp;

use chillerlan\QRCode\QRCode;
use OTPHP\TOTPInterface;

class TotpRegistrationEnvironment {
	private QRCode $qrCode;
	private TOTPInterface $totp;

	public function __construct(TOTPInterface $totp) {
		$this->totp = $totp;
		$this->qrCode = new QRCode();
	}

	public function getNativeTotp(): TOTPInterface {
		return $this->totp;
	}

	public function getSecret(): string {
		return $this->getNativeTotp()->getSecret();
	}

	public function getPrettySecret(): string {
		return chunk_split($this->getSecret(), 4, ' ');
	}

	public function getUri(): string {
		return $this->getNativeTotp()->getProvisioningUri();
	}

	public function getQrSource(): string {
		return $this->qrCode->render($this->getUri());
	}
}
