<?php

namespace App\Module\TwoFactor\Totp;

use App\Entity\User2FA;
use App\Module\TwoFactor\Exception\TwoFactorException;
use App\Module\TwoFactor\TwoFactorManager;
use OTPHP\TOTP;
use OTPHP\TOTPInterface;
use ParagonIE\ConstantTime\Base32;

class TotpManager {
	private static function generateTotpSecret(int $size): string {
		return trim(Base32::encodeUpper(random_bytes($size)), '=');
	}

	private TwoFactorManager $manager;

	public function __construct(TwoFactorManager $manager) {
		$this->manager = $manager;
	}

	private function createNativeTotp(?string $secret = null): TOTPInterface {
		$totp = TOTP::create($secret ?? self::generateTotpSecret(20)); //TODO: hardcoded
		$totp->setIssuer("ToDo List");
		$totp->setLabel($this->manager->getUser()->getUsername());
		return $totp;
	}

	public function getManager(): TwoFactorManager {
		return $this->manager;
	}

	public function createTotpRegistration(): TotpRegistrationEnvironment {
		return new TotpRegistrationEnvironment($this->createNativeTotp());
	}

	public function handleTotpRegistration(TotpRegistrationEffect $effect): void {
		$totp = $this->createNativeTotp($effect->getSecret());
		if ($totp->verify($effect->getCode(), null, 1)) {
			$this->manager->addTwoFactor($this->createTotp2FA($effect->getName(), $effect->getSecret()));
		} else {
			throw new TwoFactorException('Podany jednorazowy kod jest nieprawidłowy. Spróbuj ponowie.'); //TODO: Polish exception
		}
	}

	public function verifyTotp(string $code): ?User2FA {
		foreach ($this->manager->getUser()->getTwoFactorAuths() as $tfa) {
			if ($tfa->getMethod() === 'totp') {
				$secret = $tfa->getData()['secret'];
				$totp = $this->createNativeTotp($secret);
				if ($totp->verify($code, null, 1)) {
					return $tfa;
				}
			}
		}
		return null;
	}

	private function createTotp2FA(?string $name, string $secret): User2FA {
		return new User2FA($this->getManager()->getUser(), 'totp', $name, ['secret' => $secret]);
	}

}
