<?php

namespace App\Module\TwoFactor\U2f;

use App\Module\TwoFactor\U2f\Object\RegisterResponse;
use JMS\Serializer\Annotation as Serializer;

class U2fRegistrationEffect {
	private ?string $name;
	/**
	 * @var U2fRegistrationEnvironment
	 */
	private $registerEnvironment;
	/**
	 * @var RegisterResponse
	 */
	private $registerResponse;

	/**
	 * U2fRegistrationEffect constructor.
	 * @param U2fRegistrationEnvironment $registerEnvironment
	 * @param RegisterResponse $registerResponse
	 */
	public function __construct(?string $name, $registerEnvironment, $registerResponse) {
		$this->name = $name;
		$this->registerEnvironment = $registerEnvironment;
		$this->registerResponse = $registerResponse;
	}

	public function getName(): ?string {
		return $this->name;
	}

	/**
	 * @return U2fRegistrationEnvironment
	 */
	public function getRegisterEnvironment() {
		return $this->registerEnvironment;
	}

	/**
	 * @return RegisterResponse
	 */
	public function getRegisterResponse() {
		return $this->registerResponse;
	}
}
