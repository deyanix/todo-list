<?php

namespace App\Module\TwoFactor\U2f;

use App\Entity\User2FA;
use App\Module\TwoFactor\Exception\TwoFactorException;
use App\Module\TwoFactor\TwoFactorManager;
use App\Module\TwoFactor\U2f\Object\Registration;
use u2flib_server\Error as U2fException;
use u2flib_server\U2F;

class U2fManager {
	private TwoFactorManager $manager;
	private U2F $u2f;

	public function __construct(TwoFactorManager $manager) {
		$this->manager = $manager;
		$this->u2f = self::createNativeU2f();
	}

	private function createNativeU2f(): U2F {
		$scheme = isset($_SERVER['HTTPS']) ? "https://" : "http://";
		return new U2F($scheme . $_SERVER['HTTP_HOST']);
	}

	public function getManager(): TwoFactorManager {
		return $this->manager;
	}

	public function createU2fRegistration(): U2fRegistrationEnvironment {
		list($registerRequest, $signRequests) = $this->u2f->getRegisterData($this->getRegistrations()); //TODO empty array of regs; Object from library
		return new U2fRegistrationEnvironment($registerRequest, $signRequests);
	}

	public function handleU2fRegistration(U2fRegistrationEffect $effect): void {
		try {
			$data = $this->u2f->doRegister($effect->getRegisterEnvironment()->getRegisterRequest(), $effect->getRegisterResponse()); //TODO Object from library
			$this->getManager()->addTwoFactor($this->createU2f2FA($effect->getName(), $data));
		} catch (U2fException $error) {
			throw new TwoFactorException('Błędna weryfikacja klucza sprzętowego. Spróbuj ponowie.');
		}
	}

	private function createU2f2FA(?string $name, $data): User2FA {
		return new User2FA($this->getManager()->getUser(), 'u2f', $name, json_decode(json_encode($data), true)); //TODO ugly
	}

	public function getRegistrations(): array {
		$registrations = [];
		foreach ($this->getManager()->getUser()->getTwoFactorAuths() as $tfa) {
			if ($tfa->getMethod() === 'u2f') {
				$data = $tfa->getData();
				$registration = new Registration();
				$registration->counter = intval($data['counter']);
				$registration->keyHandle = $data['keyHandle'];
				$registration->publicKey = $data['publicKey'];
				$registration->certificate = $data['certificate'];
				$registrations[] = $registration;
			}
		}
		return $registrations;
	}
}
