<?php

namespace App\Module\TwoFactor\U2f;

use App\Module\TwoFactor\TwoFactorUtil;
use App\Module\TwoFactor\U2f\Object\RegisterRequest;
use App\Module\TwoFactor\U2f\Object\SignRequest;
use JMS\Serializer\Annotation as Serializer;

class U2fRegistrationEnvironment {
	/**
	 * @var RegisterRequest
	 * @Serializer\Type("App\Module\TwoFactor\U2f\Object\RegisterRequest")
	 */
	private $registerRequest;
	/**
	 * @var SignRequest[]
	 * @Serializer\Type("array<App\Module\TwoFactor\U2f\Object\SignRequest>")
	 */
	private $signRequests;

	/**
	 * U2fRegistrationEnvironment constructor.
	 * @param $registerRequest
	 * @param $signRequests
	 */
	public function __construct($registerRequest, $signRequests) {
		$this->registerRequest = $registerRequest;
		$this->signRequests = $signRequests;
	}

	/**
	 * @return RegisterRequest
	 */
	public function getRegisterRequest() {
		return $this->registerRequest;
	}

	/**
	 * @return SignRequest[]
	 */
	public function getSignRequests() {
		return $this->signRequests;
	}
}
