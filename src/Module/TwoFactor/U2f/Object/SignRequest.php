<?php

namespace App\Module\TwoFactor\U2f\Object;

class SignRequest {
	public string $version;
	public string $challenge;
	public string $keyHandle;
	public string $appId;
}
