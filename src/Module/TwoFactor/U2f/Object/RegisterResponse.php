<?php

namespace App\Module\TwoFactor\U2f\Object;

class RegisterResponse {
	public string $version;
	public string $clientData;
	public string $keyHandle;
	public string $registrationData;
}
