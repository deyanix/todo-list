<?php

namespace App\Module\TwoFactor\U2f\Object;

class RegisterRequest {
	public string $version;
	public string $challenge;
	public string $appId;
}
