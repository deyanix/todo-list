<?php

namespace App\Module\TwoFactor\U2f\Object;

class Registration {
	public string $keyHandle;
	public string $publicKey;
	public string $certificate;
	public int $counter;


}
