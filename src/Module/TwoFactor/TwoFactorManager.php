<?php

namespace App\Module\TwoFactor;

use App\Entity\User;
use App\Entity\User2FA;
use App\Module\TwoFactor\BackupCode\BackupCodeManager;
use App\Module\TwoFactor\Exception\TwoFactorException;
use App\Module\TwoFactor\Totp\TotpManager;
use App\Module\TwoFactor\Totp\TotpRegistrationEnvironment;
use App\Module\TwoFactor\Totp\TotpRegistrationEffect;
use App\Module\TwoFactor\U2f\U2fManager;
use App\Module\TwoFactor\U2f\U2fRegistrationEffect;
use App\Module\TwoFactor\U2f\U2fRegistrationEnvironment;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use OTPHP\TOTP;
use OTPHP\TOTPInterface;
use ParagonIE\ConstantTime\Base32;
use Symfony\Component\Security\Core\Security;
use u2flib_server\Error as U2fException;
use u2flib_server\U2F;

final class TwoFactorManager {
	private EntityManagerInterface $entityManager;
	private User $user;

	private BackupCodeManager $backupCodeManager;
	private TotpManager $totpManager;
	private U2fManager $u2fManager;

	public function __construct(EntityManagerInterface $entityManager, Security $security) {
		$this->entityManager = $entityManager;

		$user = $security->getUser();
		if (!$user instanceof User) {
			throw new Exception('Wrong user object'); //TODO: exception
		}
		$this->user = $user;

		$this->backupCodeManager = new BackupCodeManager($this);
		$this->totpManager = new TotpManager($this);
		$this->u2fManager = new U2fManager($this);
	}

	public function getUser(): User {
		return $this->user;
	}

	public function getBackupCodeManager(): BackupCodeManager {
		return $this->backupCodeManager;
	}

	public function getTotpManager(): TotpManager {
		return $this->totpManager;
	}

	public function getU2fManager(): U2fManager {
		return $this->u2fManager;
	}

	private function clearTwoFactorAuths() {
		foreach ($this->getUser()->getTwoFactorAuths() as $tfa) { //TODO ugly, pls sql
			$this->entityManager->remove($tfa);
		}
	}

	public function addTwoFactor(User2FA $user2FA): void {
		if (!$this->getUser()->isTwoFactorEnabled()) {
			$this->enableTwoFactor();
		}
		$this->entityManager->persist($user2FA);
		$this->entityManager->flush();
	}

	public function removeTwoFactor(User2FA $user2FA): void {
		if ($user2FA->getMethod() !== 'backup_code') {
			$this->entityManager->remove($user2FA);
			$this->entityManager->flush();
		}
	}

	public function enableTwoFactor(): void {
		if (!$this->getUser()->isTwoFactorEnabled()) {
			$this->clearTwoFactorAuths();
			$this->getUser()->setTwoFactorEnabled(true);
			$this->getBackupCodeManager()->createBackupCodes();
			$this->entityManager->flush();
		}
	}

	public function disableTwoFactor(): void {
		if ($this->getUser()->isTwoFactorEnabled()) {
			$this->clearTwoFactorAuths();
			$this->getUser()->setTwoFactorEnabled(false);
			$this->entityManager->flush();
		}
	}
}
