<?php

namespace App\Module\TwoFactor\BackupCode;

use App\Entity\User2FA;
use App\Module\TwoFactor\TwoFactorManager;

class BackupCodeManager {
	private static function generateString(int $length): string {
		static $alphabet = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$token = '';
		for ($i = 0; $i < $length; $i++) {
			$token .= $alphabet[mt_rand(0, strlen($alphabet) - 1)];
		}
		return $token;
	}

	private static function generateCodes(int $amount, int $length): array {
		$codes = [];
		$i = 0;
		while ($i < $amount) {
			$code = self::generateString($length);
			if (!array_search($code, $codes)) {
				$codes[$i++] = $code;
			}
		}
		return $codes;
	}

	private TwoFactorManager $manager;

	public function __construct(TwoFactorManager $manager) {
		$this->manager = $manager;
	}

	public function getManager(): TwoFactorManager {
		return $this->manager;
	}

	public function createBackupCodes(): void {
		$this->getManager()->addTwoFactor($this->createBackupCodes2FA(10, 6)); //TODO: hardcoded values
	}

	public function verifyBackupCode(string $code): ?User2FA {
		foreach ($this->getManager()->getUser()->getTwoFactorAuths() as $tfa) {
			if ($tfa->getMethod() === 'backup_code' && in_array($code, $tfa->getData())) {
				return $tfa;
			}
		}
		return null;
	}

	public function authenticateBackupCode(string $code, User2FA $tfa): void {
		$data = $tfa->getData();
		array_splice($data, array_search($code, $data), 1);
		$tfa->setData($data);
		$this->getManager()->addTwoFactor($tfa);
	}

	private function createBackupCodes2FA(int $amount, int $length): User2FA {
		return new User2FA($this->getManager()->getUser(), 'backup_code', null, self::generateCodes($amount, $length));
	}
}
