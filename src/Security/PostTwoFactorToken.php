<?php

namespace App\Security;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;
use Symfony\Component\Security\Guard\Token\GuardTokenInterface;

class PostTwoFactorToken extends AbstractToken implements GuardTokenInterface {
	private string $method;

	public function __construct(TwoFactorToken $token, string $method) {
		parent::__construct($token->getUser()->getRoles());
		$this->setUser($token->getUser());
		$this->method = $method;

		$this->setAuthenticated(true);
	}

	public function isTwoFactorEnabled() {
		return $this->getUser()->isTwoFactorEnabled();
	}

	public function getTwoFactorMethod(): string {
		return $this->method;
	}

	public function getCredentials() {
		return '';
	}
}
