<?php

namespace App\Security;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;
use Symfony\Component\Security\Guard\Token\GuardTokenInterface;

class TwoFactorToken extends AbstractToken implements GuardTokenInterface {
	public function __construct(User $user) {
		parent::__construct($user->getRoles());
		$this->setUser($user);
		$this->setAuthenticated(true);
	}

	public function isTwoFactorEnabled() {
		return $this->getUser()->isTwoFactorEnabled();
	}

	public function getCredentials() {
		return '';
	}
}
