<?php

namespace App\Security;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class TwoFactorTrustResolver implements AuthenticationTrustResolverInterface {
	private AuthenticationTrustResolverInterface $decoratedTrustResolver;

	public function __construct(AuthenticationTrustResolverInterface $decoratedTrustResolver) {
		$this->decoratedTrustResolver = $decoratedTrustResolver;
	}

	public function isAnonymous(TokenInterface $token = null) {
		return $this->decoratedTrustResolver->isAnonymous($token);
	}

	public function isRememberMe(TokenInterface $token = null) {
		return $this->decoratedTrustResolver->isRememberMe($token);
	}

	public function isFullFledged(TokenInterface $token = null) {
		return (!$token instanceof TwoFactorToken || !$token->isTwoFactorEnabled()) && $this->decoratedTrustResolver->isFullFledged($token);
	}
}
