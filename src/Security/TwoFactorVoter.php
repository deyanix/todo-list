<?php

namespace App\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

class TwoFactorVoter implements VoterInterface {
	public const IS_AUTHENTICATED_2FA_IN_PROGRESS = 'IS_AUTHENTICATED_2FA_IN_PROGRESS';
	public const IS_AUTHENTICATED_WITH_2FA = 'IS_AUTHENTICATED_WITH_2FA';

	public function vote(TokenInterface $token, $subject, array $attributes) {
		if (!$token instanceof TwoFactorToken) {
			return VoterInterface::ACCESS_ABSTAIN;
		}

		foreach ($attributes as $attribute) {
			if ($attribute === self::IS_AUTHENTICATED_2FA_IN_PROGRESS) {
				return VoterInterface::ACCESS_GRANTED;
			}
			if ($attribute === self::IS_AUTHENTICATED_WITH_2FA) {
				return VoterInterface::ACCESS_ABSTAIN;
			}
		}

		return VoterInterface::ACCESS_ABSTAIN;
	}
}
