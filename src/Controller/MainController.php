<?php

namespace App\Controller;

use App\Entity\Task;
use App\Entity\User;
use App\Entity\User2FA;
use App\Module\TwoFactor\Totp\TotpRegistrationEffect;
use App\Module\TwoFactor\TwoFactorManager;
use App\Module\TwoFactor\U2f\Object\RegisterResponse;
use App\Module\TwoFactor\U2f\U2fRegistrationEffect;
use App\Module\TwoFactor\U2f\U2fRegistrationEnvironment;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use OTPHP\TOTP;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use u2flib_server\U2F;

class MainController extends AbstractController {

	private EntityManagerInterface $entityManager;

	public function __construct(EntityManagerInterface $entityManager) {
		$this->entityManager = $entityManager;
	}

	/**
	 * @Route("/", name="app_main")
	 */
	public function main(): Response {
		$tasks = $this->entityManager->getRepository(Task::class)->findBy(['user' => $this->getUser()]);

		return $this->render('task/task-manager.html.twig', [
			'tasks' => $tasks
		]);
	}

	/**
	 * @Route("/task/create", name="app_task_create")
	 */
	public function createTask(Request $request): Response {
		if ($request->isMethod('POST')) {
			$name = $request->get('name');
			$description = $request->get('description');

			if (empty(trim($name))) {
				return $this->render('task/task-editor.html.twig', [
					'new' => true,
					'name' => $name,
					'description' => $description,
					'success' => false,
					'alert' => 'Nazwa nie może być pusta'
				]);
			}

			$task = new Task();
			$task->setUser($this->getUser());
			$task->setName($name);
			$task->setDescription($description);
			$this->entityManager->persist($task);
			$this->entityManager->flush();
			return $this->redirectToRoute('app_main');
		}

		return $this->render('task/task-editor.html.twig', [
			'new' => true
		]);
	}

	/**
	 * @Route("/task/{id<\d+>}", name="app_task_preview")
	 */
	public function previewTask(int $id): Response {
		$task = $this->entityManager->getRepository(Task::class)->find($id);
		if (!$task instanceof Task || $task->getUser() !== $this->getUser()) {
			return $this->redirectToRoute('app_main');
		}
		return $this->render('task/task-preview.html.twig', [
			'task' => $task
		]);
	}

	/**
	 * @Route("/task/{id<\d+>}/delete", name="app_task_delete")
	 */
	public function deleteTask(int $id): Response {
		$task = $this->entityManager->getRepository(Task::class)->find($id);
		if ($task instanceof Task && $task->getUser() === $this->getUser()) {
			$this->entityManager->remove($task);
			$this->entityManager->flush();
		}

		return $this->redirectToRoute('app_main');
	}

	/**
	 * @Route("/task/{id<\d+>}/edit", name="app_task_edit")
	 */
	public function editTask(int $id, Request $request): Response {
		$task = $this->entityManager->getRepository(Task::class)->find($id);
		if (!$task instanceof Task || $task->getUser() !== $this->getUser()) {
			return $this->redirectToRoute('app_main');
		}

		if ($request->isMethod('POST')) {
			$name = $request->get('name');
			$description = $request->get('description');

			if (empty(trim($name))) {
				return $this->render('task/task-editor.html.twig', [
					'new' => false,
					'name' => $name,
					'description' => $description,
					'success' => false,
					'alert' => 'Nazwa nie może być pusta'
				]);
			}

			$task->setName($name);
			$task->setDescription($description);
			$this->entityManager->persist($task);
			$this->entityManager->flush();
			return $this->redirectToRoute('app_main');
		}

		return $this->render('task/task-editor.html.twig', [
			'new' => false,
			'name' => $task->getName(),
			'description' => $task->getDescription()
		]);
	}

	/**
	 * @Route("/change-password", name="app_change_password")
	 */
	public function changePassword(Request $request, UserPasswordEncoderInterface $encoder) {
		if ($request->isMethod('POST')) {
			$user = $this->getUser();
			$oldPassword = $request->request->get('old_password');
			$newPassword = $request->request->get('new_password');
			$repeatedPassword = $request->request->get('repeated_password');

			if ($newPassword !== $repeatedPassword) {
				return $this->render('account/change-password.html.twig', [
					'success' => false,
					'alert' => 'Powtórzone hasło jest nieprawidłowe.'
				]);
			}
			if (!$user instanceof User) {
				return $this->render('account/change-password.html.twig', [
					'success' => false,
					'alert' => 'Błąd wewnętrzny aplikacji. Proszę się zalogować ponownie.'
				]);
			}
			if (!$encoder->isPasswordValid($user, $oldPassword)) {
				return $this->render('account/change-password.html.twig', [
					'success' => false,
					'alert' => 'Stare hasło jest nieprawidłowe.'
				]);
			}
			$user->setPassword($encoder->encodePassword($user, $newPassword));
			$this->entityManager->persist($user);
			$this->entityManager->flush();

			return $this->render('account/change-password.html.twig', [
				'success' => true,
				'alert' => 'Hasło zaktualizowano.'
			]);
		}

		return $this->render('account/change-password.html.twig');
	}

	/**
	 * @Route("/2fa", name="app_2fa_manager")
	 */
	public function twoFactorManager(Request $request, TwoFactorManager $manager) {
		if ($request->getMethod() === 'POST') {
			$action = $request->get('action');
			$id = $request->request->getInt('identifier');
			$tfa = $this->entityManager->getReference(User2FA::class, $id);

			$user = $this->getUser();
			if ($user instanceof User) {
				switch ($action) {
					case 'disable':
						$manager->disableTwoFactor();
						break;
					case 'delete':
						$manager->removeTwoFactor($tfa);
						break;
					case 'preview':
						return $this->redirectToRoute('app_2fa_preview', ['id' => $id]);
				}
			}
		}

		return $this->render('account/2fa-manager.html.twig', [
			'two_factor_auths' => $this->getUser()->getTwoFactorAuths()
		]);
	}

	/**
	 * @Route("/2fa/{id<\d+>}/preview", name="app_2fa_preview")
	 */
	public function twoFactorPreview(int $id) {
		$tfa = $this->entityManager->getRepository(User2FA::class)->find($id);
		if ($tfa instanceof User2FA && $tfa->getUser() === $this->getUser()) {
			return $this->render('account/2fa-preview.html.twig', [
				'tfa' => $tfa
			]);
		}
		return $this->redirectToRoute('app_2fa_manager');
	}

	/**
	 * @Route("/2fa/configure", name="app_2fa_configure")
	 */
	public function twoFactorConfigure() {
		return $this->render('account/2fa-configure.html.twig');
	}

	/**
	 * @Route("/2fa/configure/totp", name="app_2fa_configure_totp")
	 */
	public function twoFactorConfigureTotp(Request $request, TwoFactorManager $manager) {
		$totpManager = $manager->getTotpManager();
		if ($request->isMethod('POST')) {
			$name = $request->get('name');
			$secret = $request->get('secret');
			$code = $request->get('code');
			$totpEffect = new TotpRegistrationEffect($name, $secret, $code);
			$totpManager->handleTotpRegistration($totpEffect); //TODO handle exception
		}

		$totpEnvironment = $totpManager->createTotpRegistration();
		return $this->render('account/2fa-configure-totp.html.twig', [
			'totp_secret' => $totpEnvironment->getPrettySecret(),
			'totp_qr_source' => $totpEnvironment->getQrSource()
		]);
	}

	/**
	 * @Route("/2fa/configure/u2f", name="app_2fa_configure_u2f")
	 */
	public function twoFactorConfigureU2f(Request $request, TwoFactorManager $manager) {
		$u2fManager = $manager->getU2fManager();
		if ($request->getMethod() === 'POST') {
			$name = $request->get('name');
			$registerEnvironmentText = $request->get('register_data');
			$registerResponseText = $request->get('register_response');

			$registerEnvironment = self::decodeObject($registerEnvironmentText, U2fRegistrationEnvironment::class);
			$registerResponse = self::decodeObject($registerResponseText, RegisterResponse::class);

			$registerEffect = new U2fRegistrationEffect($name, $registerEnvironment, $registerResponse);
			$u2fManager->handleU2fRegistration($registerEffect); //TODO handle exception
			return $this->redirectToRoute('app_2fa_manager');
		}

		return $this->render('account/2fa-configure-u2f.html.twig', [
			'register_data' => self::encodeObject($u2fManager->createU2fRegistration())
		]);
	}

	private static function createSerializer(): Serializer {
		return SerializerBuilder::create()
			->setPropertyNamingStrategy(new IdenticalPropertyNamingStrategy())
			->build();
	}

	private static function encodeObject(object $object): string {
		return base64_encode(self::createSerializer()->serialize($object, 'json')); //todo: single serializer
	}

	private static function decodeObject(string $object, string $class): object {
		return self::createSerializer()->deserialize(base64_decode($object), $class, 'json'); //todo: single serializer
	}
}
