<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\User2FA;
use App\Module\TwoFactor\TwoFactorManager;
use App\Security\PostTwoFactorToken;
use App\Security\TwoFactorToken;
use Doctrine\ORM\EntityManagerInterface;
use OTPHP\TOTP;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use u2flib_server\Registration;
use u2flib_server\U2F;

/**
 * @Route(name="app_security_")
 */
class SecurityController extends AbstractController {

	private EntityManagerInterface $entityManager;

	public function __construct(EntityManagerInterface $entityManager) {
		$this->entityManager = $entityManager;
	}

	/**
	 * @Route("/login", name="login")
	 */
	public function login(AuthenticationUtils $authenticationUtils): Response {
		$error = $authenticationUtils->getLastAuthenticationError();
		$lastUsername = $authenticationUtils->getLastUsername();

		return $this->render('security/login.html.twig', [
			'error' => $error,
			'last_username' => $lastUsername
		]);
	}

	/**
	 * @Route("/login/2fa/choice", name="2fa_choice")
	 */
	public function tfaChoice(): Response {
		return $this->render('security/2fa-choice.html.twig');
	}

	/**
	 * @Route("/login/2fa/totp", name="2fa_totp")
	 */
	public function tfaTotp(Request $request, TokenStorageInterface $tokenStorage, TwoFactorManager $manager): Response {
		$totpManager = $manager->getTotpManager();
		if ($request->getMethod() === 'POST') {
			$code = $request->get('code');
			$token = $tokenStorage->getToken();
			if ($token instanceof TwoFactorToken) {
				if ($tfa = $totpManager->verifyTotp($code)) {
					$tokenStorage->setToken(new PostTwoFactorToken($token, 'totp'));
					return $this->redirectToRoute('app_main');
				} else {
					$this->addFlash('danger', 'Błędny kod weryfikacji dwuetapowej.');
				}
			} else {
				$this->addFlash('danger', 'Błąd autoryzacji. Spróbuj zalogować się ponownie.');
			}
		}
		return $this->render('security/2fa-totp.html.twig');
	}

	/**
	 * @Route("/login/2fa/backup-code", name="2fa_backup_code")
	 */
	public function tfaBackupCode(Request $request, TokenStorageInterface $tokenStorage, TwoFactorManager $manager): Response {
		$backupCodeManager = $manager->getBackupCodeManager();
		if ($request->getMethod() === 'POST') {
			$code = $request->get('code');
			$token = $tokenStorage->getToken();
			if ($token instanceof TwoFactorToken) {
				if ($tfa = $backupCodeManager->verifyBackupCode($code)) {
					$backupCodeManager->authenticateBackupCode($code, $tfa);
					$tokenStorage->setToken(new PostTwoFactorToken($token, 'backup_code'));
					return $this->redirectToRoute('app_main');
				} else {
					$this->addFlash('danger', 'Błędny kod weryfikacji dwuetapowej.');
				}
			} else {
				$this->addFlash('danger', 'Błąd autoryzacji. Spróbuj zalogować się ponownie.');
			}
		}
		return $this->render('security/2fa-backup-code.html.twig');
	}

	/**
	 * @Route("/login/2fa/u2f", name="2fa_u2f")
	 */
	public function tfaU2f(Request $request, TokenStorageInterface $tokenStorage, TwoFactorManager $manager): Response {
		$scheme = isset($_SERVER['HTTPS']) ? "https://" : "http://";
		$u2f = new U2F($scheme . $_SERVER['HTTP_HOST']);

		$token = $tokenStorage->getToken();
		$user = $token->getUser();
		$sigs = [];
		if ($user instanceof User) {
			if ($token instanceof TwoFactorToken) {
				$sigs = $manager->getU2fManager()->getRegistrations();
			}

			if ($request->getMethod() === 'POST') {
				$authenticateDataText = $request->get('authenticate_data');
				$authenticateResponseText = $request->get('authenticate_response');
				if (!empty($authenticateDataText) && !empty($authenticateResponseText)) {
					$authenticateData = json_decode(base64_decode($authenticateDataText));
					$authenticateResponse = json_decode(base64_decode($authenticateResponseText));
					$data = $u2f->doAuthenticate($authenticateData, $sigs, $authenticateResponse);
					if ($data instanceof Registration) {
						$tokenStorage->setToken(new PostTwoFactorToken($token, 'u2f'));
						return $this->redirectToRoute('app_main');
					}
				}
			}
		}

		$data = $u2f->getAuthenticateData($sigs);
		$authenticateData = base64_encode(json_encode($data));

		return $this->render('security/2fa-u2f.html.twig', [
			'authenticate_data' => $authenticateData
		]);
	}

	/**
	 * @Route("/logout", name="logout")
	 */
	public function logout() {
		throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
	}

	public function arrayToStdClass(array $array) {
		$object = new \stdClass();

		foreach ($array as $key => $value) {
			if (is_array($value)) {
				$value = $this->arrayToStdClass($value);
			}
			$object->$key = $value;
		}
		return $object;
	}
}
