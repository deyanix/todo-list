<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_2fa")
 */
class User2FA {
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer", options={"unsigned"=true})
	 */
	private int $id;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="twoFactorAuths")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private User $user;

	/**
	 * @ORM\Column(type="string")
	 */
	private string $method;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private ?string $name;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private DateTimeInterface $registerDate;

	/**
	 * @ORM\Column(type="json")
	 */
	private array $data;

	public function __construct(User $user, string $method, ?string $name, array $data) {
		$this->user = $user;
		$this->method = $method;
		$this->name = $name;
		$this->data = $data;
		$this->registerDate = new DateTime('now');
	}

	public function getId(): int {
		return $this->id;
	}

	public function getUser(): User {
		return $this->user;
	}

	public function setUser(User $user): void {
		$this->user = $user;
	}

	public function getMethod(): string {
		return $this->method;
	}

	public function setMethod(string $method): void {
		$this->method = $method;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName(?string $name): void {
		$this->name = $name;
	}

	public function getRegisterDate(): DateTimeInterface {
		return $this->registerDate;
	}

	public function setRegisterDate(DateTimeInterface $registerDate): void {
		$this->registerDate = $registerDate;
	}

	public function getData(): array {
		return $this->data;
	}

	public function setData(array $data): void {
		$this->data = $data;
	}
}
