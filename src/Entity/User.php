<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 */
class User implements UserInterface, \Serializable {
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer", options={"unsigned"=true})
	 */
	private int $id;

	/**
	 * @ORM\Column(type="string", unique=true)
	 */
	private string $username;

	/**
	 * @ORM\Column(type="string")
	 */
	private string $password;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private bool $twoFactorEnabled = false;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\User2FA", mappedBy="user")
	 */
	private Collection $twoFactorAuths;

	public function __construct() {
		$this->twoFactorAuths = new ArrayCollection();
	}

	public function getId() {
		return $this->id;
	}

	public function getUsername() {
		return $this->username;
	}

	public function setPassword(string $password): void {
		$this->password = $password;
	}

	public function getPassword() {
		return $this->password;
	}

	public function setTwoFactorEnabled(bool $twoFactorEnabled): void {
		$this->twoFactorEnabled = $twoFactorEnabled;
	}

	public function isTwoFactorEnabled(): bool {
		return $this->twoFactorEnabled;
	}

	public function getTwoFactorAuths(): array {
		return $this->twoFactorAuths->toArray();
	}

	public function getRoles() {
		return [];
	}

	public function getSalt() {
		return null;
	}

	public function eraseCredentials() {
		// Nothing.
	}

	public function serialize() {
		return serialize([
			$this->id,
			$this->username,
			$this->password
		]);
	}

	public function unserialize($serialized) {
		list(
			$this->id,
			$this->username,
			$this->password
		) = unserialize($serialized);
	}
}
