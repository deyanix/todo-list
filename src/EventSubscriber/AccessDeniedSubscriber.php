<?php

namespace App\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class AccessDeniedSubscriber implements EventSubscriberInterface {
	protected $router;
	protected $security;
	protected $dispatcher;

	public static function getSubscribedEvents() {
		return [
			KernelEvents::EXCEPTION => ['onKernelException', 2]
		];
	}

	public function __construct(RouterInterface $router, Security $security) {
		$this->router = $router;
		$this->security = $security;
	}

	public function onKernelException(ExceptionEvent $event) {
		$exception = $event->getThrowable();
		if (!$exception instanceof AccessDeniedException) {
			return;
		}
		$event->setResponse(new RedirectResponse($this->router->generate('app_security_2fa_choice')));

		if ($this->security->isGranted('IS_ANONYMOUS')) {
			$response = new RedirectResponse($this->router->generate('app_security_login'));
		} else if ($this->security->isGranted('IS_AUTHENTICATED_2FA_IN_PROGRESS')) {
			$response = new RedirectResponse($this->router->generate('app_security_2fa_choice'));
		} else {
			$response = new RedirectResponse($this->router->generate('app_main'));
		}
		$event->setResponse($response);
	}
}
